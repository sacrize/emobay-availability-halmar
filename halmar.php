<?php
set_time_limit(0);

const LINK = "http://www.halmar.pl/towary.xml";

$halmar = file_get_contents(LINK);
$xml = simplexml_load_string($halmar);

if($xml === false)
{
  echo "Failed loading XML\n";
  foreach(libxml_get_errors() as $error)
  {
    echo "\t", $error->message;
  }
}

$db = new Mysqli("", "", "", "");

if ($db->connect_error) {
  die('Connect Error (' . $db->connect_errno . ') ' . $db->connect_error);
}

foreach($xml as $towar)
{
  $ean = (string)$towar->twr_ean;

  if(is_numeric($ean))
  {
    $status = status($towar);
    $dost = $towar->dost_data;
    print $ean . "\n";

    $sql = "UPDATE `products` SET `products_availability_id` = {$status}";

    if($status == 9 && strlen($dost) > 3)
    {
      $sql .= ", `products_date_available` = '{$dost}'";
    }

    $sql .= " WHERE `products_ean` = '{$ean}'";

    $db->query($sql);
  }
}

$db->close();

function status($towar)
{
  switch(strtolower($towar->twr_dostepny))
  {
    case "brak":

    if(strpos($towar->twr_kod, "####"))
    {
      return 10; // Niedostępny zawsze
    }

    return 9; // Niedostępny

    case "dostepny":

    return 4; // Dostępny

    default:

    return 11; // Niski stan magazynowy
  }
}
